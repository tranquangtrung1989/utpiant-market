<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="conn.SqlQuery, java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>UTP second hands book for sale on campus (SHBSC).</title>
<link href="css/mgc3.base.v2.css" rel="stylesheet"/>
<link href="css/style.css" rel="stylesheet"/>
<link rel="icon" href="images/home.png" type="image/x-icon" />
<link rel="shortcut icon" href="images/home.png" type="image/x-icon" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#submitButton").click(function(){
		$("#warnSms").html("");
		$("#email").val($.trim($("#email").val()));
		if($("#email").val().length==0){
			$("#warnSms").html("Please enter email address");
			return;
		}
		$("#dispName").val($.trim($("#dispName").val()));
		if($("#dispName").val().length==0){
			$("#warnSms").html("Please enter display name");
			return;
		}
		<%if((UserInfo) session.getAttribute("UserInfo")!=null){%>
			$("#currentPassword").val($.trim($("#currentPassword").val()));
			$("#newPassword").val($.trim($("#newPassword").val()));
			if($("#currentPassword").val().length<6){
				$("#warnSms").html("Current password must be at least 6 characters");
				return;
			}
			if($("#newPassword").val().length<6){
				$("#warnSms").html("New password must be at least 6 characters");
				return;
			}
		<%}else{%>
			$("#password").val($.trim($("#password").val()));
			if($("#password").val().length<6){
				$("#warnSms").html("Password must be at least 6 characters");
				return;
			}
		<%}%>
		if($("#warnSms").html()=="")
			$("#form1").submit();
	});
   $("#email").change(function(){
	   var email=$(this).val();
	   if(email.length>0){
		   if(email.indexOf("@")<=0||email.replace("@","").indexOf("@")>=0){
			   $("#warnSms").html("Incorrect email format");
			   return;
		   }
		   if(email.indexOf(".")<=0||email.indexOf(".")==email.length-1){
			   $("#warnSms").html("Incorrect email format");
			   return;
		   }
		   $.ajax({cache:false});
		   $.get("<%=request.getContextPath()%>/ajax/utp-validate-email.jsp?email="+email,function(result){
			   $("#warnSms").html(result);
			   if(result.length==0){
				   
			   }
		   });
	   }
   });
});
</script>
</head>
<body>
    <div class="mgc" style="margin-left:120px;">
<%@ include file="header.jsp" %>
<%
String email="";
String dispNme="";
String day="0";
String month="0";
String year="0";
String gender="";
if (uif != null) {
	email=uif.getEmail();
	dispNme=uif.getUserID();
	String sql="select syu_usr_gender, syu_usr_birth_dte from sysusr where syu_usr_no='"+uif.getUserNo()+"' ";
	List<String[]> rs=SqlQuery.sqlSelect(sql);
	if(rs.size()>0){
		gender=rs.get(0)[0];
		String [] birthDte=rs.get(0)[1].split("-");
		day=birthDte[2];
		month=birthDte[1];
		year=birthDte[0];
	}
}
String warnSms=request.getParameter("warnSms");
if(warnSms==null) warnSms="&nbsp;";
else if(warnSms.equals("email"))warnSms="This email address has already existed";
else if(warnSms.equals("password"))warnSms="Invalid current password";
%>
      <div class="mgc-body">
      <form id="form1" enctype="multipart/form-data" method="post" action="AccountUser">
        <table id="result">
				<tr><td id="warnSms" style="color:red"><%=warnSms %></td></tr>
				<tr>
					<td>E-mail address:<br/><input type="text" style="width:270px" class="textInp" id="email" name="email" value="<%=email %>" maxlength="50"/>&nbsp;&nbsp;&nbsp;<img src="images/login_ico.png"/></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<%if(uif!=null){%>
				<tr>
					<td>Current Password:<br/><input type="password" style="width:270px" class="textInp" id="currentPassword" name="currentPassword" maxlength="15"/>&nbsp;*</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>New Password:<br/><input type="password" style="width:270px" class="textInp" id="newPassword" name="newPassword" maxlength="15"/>&nbsp;*</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<%}else{%>
				<tr>
					<td>Password:<br/><input type="password" style="width:270px" class="textInp" id="password" name="password" maxlength="15"/>&nbsp;&nbsp;&nbsp;<img src="images/pass_ico.png"/></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<%} %>
				<tr>
					<td>Display name:<br/><input type="text" style="width:270px" class="textInp" id="dispName" name="dispName" value="<%=dispNme %>" maxlength="32"/>&nbsp;*&nbsp;</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						Your date of birth:<br/> 
						<select name="day">
							<option value="0">Day&nbsp;&nbsp;</option>
							<%
							for(int i=1;i<=31;i++){
								String sel="";
								if(Integer.parseInt(day)==i) sel="selected";
							%>
								<option value="<%=i%>" <%=sel %>><%=i %></option>
							<%}
	String []sel=new String[12];
	for(int i=0;i<12;i++) if(Integer.parseInt(month)==i+1) sel[i]="selected";
							%>
						</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<select name="month">
							<option value="0">Month</option>
							<option value="1" <%=sel[0] %>>January</option>
							<option value="2" <%=sel[1] %>>February</option>
							<option value="3" <%=sel[2] %>>March</option>
							<option value="4" <%=sel[3] %>>April</option>
							<option value="5" <%=sel[4] %>>May</option>
							<option value="6" <%=sel[5] %>>June</option>
							<option value="7" <%=sel[6] %>>July</option>
							<option value="8" <%=sel[7] %>>August</option>
							<option value="9" <%=sel[8] %>>September</option>
							<option value="10" <%=sel[9] %>>October</option>
							<option value="11" <%=sel[10] %>>November</option>
							<option value="12" <%=sel[11] %>>December</option>
						</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<select name="year">
							<option value="0">Year&nbsp;&nbsp;</option>
							<%
							for(int i=2000;i>=1900;i--){
								String sely="";
								if(Integer.parseInt(year)==i) sely="selected";
							%>
								<option value="<%=i%>" <%=sely %>><%=i %></option>
							<%}
							%>
						</select>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
					Gender:<br/>
					<select name="gender">
							<option value="0">&nbsp;</option>
					<%
						String male="",female="";
						if(gender.equals("M")) male="selected";
						else if(gender.equals("F")) female="selected";
					%>
							<option value="M" <%=male %>>Male</option>
							<option value="F" <%=female %>>Female</option>
						</select>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>Upload Image:<br/><input type="file" name="img"/></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td align="center">
						<%
							String actButt="Update";
							if(uif==null){
								actButt="Create";
							}%>
						<input type="button" id="submitButton" class="button" style="width:150px;height:25px" value="<%=actButt %> My Account+"/>
					</td>
				</tr>
			</table>
		</form>
      </div>
      <div style="height:100px;">&nbsp;</div>
      <div class="g-section footer">
        <p class="fineprint">
          �2012 Copy Right By Utpiant
        </p>
      </div>
    </div>
 </body>
</html>