<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="conn.SqlQuery, java.util.List,SHBSC.com.DataLocation"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>UTP second hands book for sale on campus (SHBSC).</title>
<link href="../css/mgc3.base.v2.css" rel="stylesheet"/>
<link href="../css/style.css" rel="stylesheet"/>
<link rel="stylesheet" href="../css/sold.css"/>
<link href="../css/search.css" rel="stylesheet"/>
<link href="../css/banner.css" rel="stylesheet"/>
<link rel="icon" href="../images/home.png" type="image/x-icon" />
<link rel="shortcut icon" href="../images/home.png" type="image/x-icon" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#create").click(function(){
		if($("#isLogIn").val()=="false"){
			$("#warnSms").html("Please login to use our service");
			return;
		}
		document.location.href='utp-sold-product-create.jsp';
	});
	$(".deleteTrans").click(function(){
		$("#form_transNo"+$(this).attr("id")).submit();
	});
	$(".editTrans").click(function(){
		$("#form_transNo"+$(this).attr("id")).attr("action","utp-sold-product-create.jsp");
		$("#form_transNo"+$(this).attr("id")).submit();
	});
	$("#field").keyup(function() {
		$("#x").fadeIn();
		if ($.trim($("#field").val()) == "") {
			$("#x").fadeOut();
		}
	});
	// on click of "X", delete input field value and hide "X"
	$("#x").click(function() {
		$("#field").val("");
		$(this).hide();
	});
	$("#myAdvertisement").click(function(){
		$("#myAds").val("myAds");
		$("#form1").submit();
	});
});
</script>
</head>
<body>
    <div class="mgc" style="margin-left:120px;">
<%@ include file="../header.jsp" %>
      <div class="mgc-body">
		<%
String isLogIn="true";
String [] fieldLst=null;
if(uif==null)
	isLogIn="false";
String category=request.getParameter("category");
String field=request.getParameter("field");
String myAds=request.getParameter("myAds");
if(field!=null){
	field=field.trim();
	if(field.length()>0)
		fieldLst=field.split(" ");
}
String sel1="",sel2="",sel3="",sel4="";
if(category==null) category="0";
else if(category.equals("b")) sel1="selected";
else if(category.equals("c")) sel2="selected";
else if(category.equals("p")) sel3="selected";
else if(category.equals("s")) sel4="selected";
%>
<form id="form1" method="post" action="utp-sold-product.jsp">
<div style="padding-left:50px;width:870px;background-repeat:no-repeat;height:70px;background-image:url('../images/banner_sale_create.png');">&nbsp;</div>
<div style="width:793px;height:50px;margin-left:20px">
	<table width="100%">
		<tr><td style="color:red" colspan="2"><span id="warnSms">&nbsp;</span></td></tr>
		<tr><td>
		<input type="text" name="field" id="field" /><div id="delete"><span id="x">x</span></div>
		<select id="category" name="category" style="height:27px;float:left;">
					<option value="0">All Categories</option>
					<option value="b" <%=sel1 %>>Books</option>
					<option value="c" <%=sel2 %>>Computer/IT Hardware</option>
					<option value="p" <%=sel3 %>>Phone/Electronics</option>
					<option value="s" <%=sel4 %>>Stuffs</option>
				</select>&nbsp;
		<input type="submit" maxlength="300" size="50" name="submitSearch" id="submitSearch" value="Search" /></td>
	</tr>
	</table>
</div>
</form>
<div style="width:100%;height:10px;">&nbsp;</div>
<%
String sql="select sld_tran_nme, sld_tran_prc, sld_tran_phone, sld_tran_email, sld_tran_usr_no, sld_tran_file_nme, "+
"sld_tran_no, DATE_FORMAT(sld_lst_upd_dte, '%d/%m/%Y'), sld_tran_desc, sld_tran_typ from sltran where ";
if(myAds!=null&&myAds.equals("myAds"))
	sql+="sld_tran_usr_no='"+uif.getUserNo()+"' ";
else
{
	if(!category.equals("0")) sql+="sld_tran_typ='"+category+"' ";
	else sql+="sld_tran_typ in ('b','c','p','s') ";
	if(fieldLst!=null&&fieldLst.length>0){
	    sql+="and ( ";
		for(int i=0;i<fieldLst.length;i++){
			if(fieldLst[i].trim().length()>0){
				if(i>0) sql+="or ";
				sql+="sld_tran_nme like '%"+fieldLst[i].trim()+"%' or sld_tran_desc like '%"+fieldLst[i].trim()+"%' ";
			}
		}
	    sql+=") ";
	}
}
sql+="order by sld_tran_no desc";
List<String[]> rs=SqlQuery.sqlSelect(sql);
int size=5;
int pageNo=rs.size()%size==0?rs.size()/size:rs.size()/size+1, start, end;
String pageNum=request.getParameter("pageNo");
if(pageNum==null){
	  start=0;
	  end=rs.size()<size?rs.size():size;
}
else{
	  start=Integer.parseInt(pageNum)*size;
	  end=rs.size()<start+size?rs.size():start+size;
}
for(int i=start;i<end;i++){
	String title=rs.get(i)[0];
	String price=rs.get(i)[1];
	String phone=rs.get(i)[2];
	String email=rs.get(i)[3];
	String usrNo=rs.get(i)[4];
	String imgNme=rs.get(i)[5];
	String trnsNo=rs.get(i)[6];
	String date=rs.get(i)[7];
	String desc=rs.get(i)[8];
	String itemTyp=rs.get(i)[9];
	String imgLoc="";
	if(imgNme.length()>0)
		imgLoc="image/"+imgNme+"?location="+DataLocation.sellGood+"&usrNo="+usrNo;
	else if(itemTyp.equals("s"))
		imgLoc=contextPath+"/data_images/stuff/stuff"+Integer.parseInt(trnsNo)%8+".png";
	else if(itemTyp.equals("b"))
		imgLoc=contextPath+"/data_images/book/book"+Integer.parseInt(trnsNo)%4+".png";
	else if(itemTyp.equals("c"))
		imgLoc=contextPath+"/data_images/it/it"+Integer.parseInt(trnsNo)%7+".png";
	else if(itemTyp.equals("p"))
		imgLoc=contextPath+"/data_images/phone/phone"+Integer.parseInt(trnsNo)%7+".png";
	
%>
	<div id="showAd" style="height:150px">
		<form method="post" action="deleteSellTransaction" id="form_transNo<%=trnsNo%>">
		<table id="result" width="100%">
			<tr>
				<td width="20%" valign="bottom"><img src="<%=imgLoc%>"/>&nbsp;</td>
				<td width="60%" valign="top">
						<table border="0" width="100%">
							<tr><td colspan="3" style="color:blue"><%=title %></td></tr>
							<tr><td>Price</td><td>:</td><td><%=price %></td>
							</tr>
							<tr><td>Phone</td><td>:</td><td><%=phone %></td>
							</tr>
							<tr><td>Mail</td><td>:</td><td><%=email %></td></tr>
							<tr><td>Description</td><td>:</td><td><%=desc %></td>
							</tr>
						</table>
				</td>
				<td valign="top">
					<table>
						<tr><td>Date</td><td>:</td><td align="right"><%=date %></td></tr>
						<%if(uif!=null&&(uif.isAdmin()||uif.getUserNo().equals(usrNo))){%>
						<tr><td colspan="3" align="center">
						<input type="hidden" name="title" value="<%=title %>"/>
						<input type="hidden" name="price" value="<%=price %>"/>
						<input type="hidden" name="phone" value="<%=phone %>"/>
						<input type="hidden" name="mail" value="<%=email %>"/>
						<input type="hidden" name="desc" value="<%=desc %>"/>
						<input type="hidden" name="trnsNo" value="<%=trnsNo%>"/>
						<input type="hidden" name="imgNme" value="<%=imgNme%>"/>
						<input type="hidden" name="usrNo" value="<%=usrNo%>"/>
						<input type="hidden" name="submitTyp" id="submitTyp<%=trnsNo%>" value="edit"/>
						<input type="hidden" name="itemTyp" value="<%=itemTyp%>"/>
						<a href="#" id="<%=trnsNo%>" class="editTrans">Edit</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#" id="<%=trnsNo%>" class="deleteTrans">Delete</a>
						</td></tr>
						<%}%>
					</table>
				</td>
			</tr>
		</table>
		</form>
	</div>
<%}%>
<div id="showAd" style="height:25px;background-color:#F3F7FB">
	<table width="100%">
		<tr>
		<%
		   int nextPage=start/size+1>=pageNo?start/size:start/size+1;
		   int prevPage=start/size-1<0?start/size:start/size-1;
		%>
			<td valign="top" align="left" width="10%"><a href="?pageNo=0&category=<%=category%>" style="color:blue">First Page</a></td>
			<td width="1%"><a href="?pageNo=<%=prevPage%>&category=<%=category%>"><img src="../data_images/prev.png"/></a></td>
			<%
				for(int i=0;i<pageNo;i++){
					String style="color:blue";
					if(i==start/size) style="color:red";
					out.print("<td width=\"1%\"><a  style=\""+style+"\" href=\"?pageNo="+i+"&category="+category+"\">"+(i+1)+"</a></td>");
				}
			%>
			<td width="2%"><a href="?pageNo=<%=nextPage%>&category=<%=category%>"><img src="../data_images/next.png"/></a></td>
			<td valign="top" align="right" width="10%"><a href="?pageNo=<%=pageNo-1 %>&category=<%=category%>" style="color:blue;float:right">Last Page</a></td>
		</tr>
	</table>
</div>
      </div>
      <div style="height:100px;">&nbsp;</div>
      <div class="g-section footer">
        <p class="fineprint">
          �2012 Copy Right By Utpiant
        </p>
      </div>
    </div>
 </body>
</html>