<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="conn.SqlQuery, java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>UTP second hands book for sale on campus (SHBSC).</title>
<link href="../css/mgc3.base.v2.css" rel="stylesheet"/>
<link href="../css/style.css" rel="stylesheet"/>
<link rel="stylesheet" href="../css/sold.css"/>
<link href="../css/banner.css" rel="stylesheet"/>
<link rel="icon" href="../images/home.png" type="image/x-icon" />
<link rel="shortcut icon" href="../images/home.png" type="image/x-icon" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#submitButton").click(function(){
		$("#title").val($.trim($("#title").val()));
		if($("#title").val()==""){
			$("#warnSms").html("Please Enter Title");
			return;
		}
		$("#price").val($.trim($("#price").val()));
		if($("#price").val()==""){
			$("#warnSms").html("Please Enter Price");
			return;
		}
		$("#phone").val($.trim($("#phone").val()));
		if($("#phone").val()==""){
			$("#warnSms").html("Please Enter Phone Number");
			return;
		}
		$("#mail").val($.trim($("#mail").val()));
		$("#desc").val($.trim($("#desc").val()));
		$("#form1").submit();
	});
	$("#file_browse").change(function(){
		$("#warnSms").html("");
		var file_nme=$("#file_browse").val();
		var tail=file_nme.substr(file_nme.lastIndexOf('.') + 1).toLowerCase();
		if(tail!="jpg"&&tail!="png"&&tail!="gif"){
			$("#file_browse").val("");
			$("#file_img").html($("#orgImg").val());
			$("#removeImg").hide();
			$("#warnSms").html("Please select proper image file");
			return;
		}
		if(file_nme.length>10) file_nme="..."+file_nme.substr(file_nme.length-10);
		$("#file_img").html(file_nme);
		$("#removeImg").show();
	});
	$("#removeImg").click(function(){
		$("#file_browse").val("");
		$("#file_img").html($("#orgImg").val());
		$("#removeImg").hide();
	});
	$("#desc").keypress(function(){
		return $(this).val().length<50;
	});
});
</script>
</head>
<body>
    <div class="mgc" style="margin-left:120px;">
<%@ include file="../header.jsp" %>
<%
if(uif==null){
	response.sendRedirect(contextPath);
	return;
}
String title="", price="", phone="", mail=uif.getEmail(), desc="",imgNme="", buttn="Create+", itemTyp="s";
String selBook="", selIt="", selPhon="", selStff="", trnsNo="";
String submitTyp=request.getParameter("submitTyp");
String usrNo=uif.getUserNo();
if(submitTyp!=null){
	title=request.getParameter("title");
	price=request.getParameter("price");
	phone=request.getParameter("phone");
	mail=request.getParameter("mail");
	desc=request.getParameter("desc");
	imgNme=request.getParameter("imgNme");
	itemTyp=request.getParameter("itemTyp");
	trnsNo=request.getParameter("trnsNo");
	if(itemTyp.equals("s")) selStff="selected";
	else if(itemTyp.equals("b")) selBook="selected";
	else if(itemTyp.equals("c")) selIt="selected";
	else if(itemTyp.equals("p")) selPhon="selected";
	buttn="Update";
}
%>
      <div class="mgc-body">
    <form id="form1" enctype="multipart/form-data" method="post" action="SellCreate">
<div style="padding-left:50px;width:870px;background-repeat:no-repeat;height:70px;background-image:url('../images/banner_sale_create.png');">&nbsp;</div>
<div style="height:10px">&nbsp;</div>
	<div id="createAd" style="height:400px">
		<div style="margin-left:70px;margin-top:20px;">
			<table id="result" width="650px">
				<tr><td id="warnSms" colspan="5" style="color:red">&nbsp;</td></tr>
				<tr>
					<td width="50px" style="color:blue">Title</td>
					<td width="20px">:</td>
					<td width="250px"><input type="text" style="width:200px" maxlength="50" id="title" value="<%=title %>" name="title"/>&nbsp;*</td>
					<td width="30px">Price</td>
					<td width="20px">:</td>
					<td width="200px"><input type="text" style="width:100px" maxlength="15" id="price" name="price" value="<%=price %>"/>&nbsp;*&nbsp;
									<select name="category" style="width:120px">
										<option value="s">Category</option>
										<option value="b" <%=selBook %>>Books</option>
										<option value="c" <%=selIt %>>Computer/IT Hardware</option>
										<option value="p" <%=selPhon %>>Phone/Electronics</option>
										<option value="s" <%=selStff %>>Stuffs</option>
									</select>
					</td>
				</tr>
				<tr><td colspan="6">&nbsp;</td></tr>
				<tr><td colspan="6">&nbsp;</td></tr>
				<tr>
					<td>Phone</td>
					<td>:</td>
					<td><input type="text" style="width:200px" maxlength="15" id="phone" name="phone" value="<%=phone %>"/>&nbsp;*</td>
					<td>Email</td>
					<td>:</td>
					<td ><input type="text" style="width:250px" maxlength="50" value="<%=mail %>" name="mail" id="mail"/></td>
				</tr>
				<tr><td colspan="6">&nbsp;</td></tr>
				<tr><td colspan="6">&nbsp;</td></tr>
				<tr>
					<td>Image</td>
					<td>:</td>
					<td style="width:200px;overflow:hidden;">
						<div id="file_browse_wrapper">
							<input type="file" id="file_browse" style="cursor:pointer;" name="img"/>
					    </div>
					    <a id="file_img"><%=imgNme %></a>&nbsp;&nbsp;<a href="#" id="removeImg" style="display:none;">remove</a>
					    <input type="hidden" id="orgImg" name="orgImg" value="<%=imgNme %>"/>
					</td>
					<td>Description</td>
					<td>:</td>
					<td><textarea style="width:250px;height:50px;" name="desc" id="desc"><%=desc %></textarea></td>
				</tr>
				<tr><td colspan="6">&nbsp;</td></tr>
				<tr><td colspan="6">&nbsp;</td></tr>
				<tr>
					<td colspan="6" align="center"><a href="utp-sold-product.jsp">
						<input type="button" class="deleteButton" style="width:90px;height:25px" value="Back"/></a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" id="submitButton" class="deleteButton" style="width:90px;height:25px" value="<%=buttn%>"/>
						<input type="hidden" name="submitTyp" value="<%=submitTyp%>"/>
						<input type="hidden" name="trnsNo" value="<%=trnsNo%>"/>
						<input type="hidden" name="usrNo" value="<%=usrNo%>"/>
					</td>
				</tr>
			</table>
		</div>
	</div>
</form>
      </div>
      <div style="height:100px;">&nbsp;</div>
      <div class="g-section footer">
        <p class="fineprint">
          �2012 Copy Right By Utpiant
        </p>
      </div>
    </div>
    <div id="rightAds">
        <div id="liAds">
        		<h3><a id="h3" href="http://www.lazada.com.my/mobile-phones-uni/">For Uni Graduates Only!</a></h3>
				<div id="img">
				<img src="../data_images/ads/ad1.jpg"></div>
			<span>RM 100 off on all mobile phones. Exclusively for University Graduates. Limited time only!
			</span> 
		</div>
		<div id="liAds">
        		<h3><a id="h3" href="http://toherheart.com/en/prices-drop">Toherheart.com</a></h3>
				<div id="img">
				<img src="../data_images/ads/ad2.jpg"></div>
			<span>A gift is meant to serve a greate purpose. 
			It is to carry your heart message and deliver it along with the product.
			</span> 
		</div>
		<div id="liAds">
        		<h3><a id="h3" href="http://www.lazada.com.my/usb-flash-drives/">Need extra storage space?</a></h3>
				<div id="img">
				<img src="../data_images/ads/ad3.jpg"></div>
			<span>Increase storage space by getting your USB flash drive today. Amazing Prices. Order Now!
			</span> 
		</div>
		<div id="liAds">
        		<h3><a id="h3" href="#">Hunt For A Job?</a></h3>
				<div id="img">
				<img src="../data_images/ads/ad4.png"></div>
			<span>Various Industries of Jobs Offered. Let's Join Our Skill & Talent Network Database.
			</span> 
		</div>
    </div>
 </body>
</html>