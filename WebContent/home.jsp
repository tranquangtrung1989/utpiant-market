<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="conn.SqlQuery, java.util.List,SHBSC.com.DataLocation"%>
    <%UserInfo uif1 = (UserInfo) session.getAttribute("UserInfo"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>UTP second hands book for sale on campus (SHBSC).</title>
<link href="css/mgc3.base.v2.css" rel="stylesheet"/>
<link href="css/style.css" rel="stylesheet"/>
<link href="css/banner.css" rel="stylesheet"/>
<link href="css/search.css" rel="stylesheet"/>
<link href="css/auct.css" rel="stylesheet"/>
<link rel="icon" href="images/home.png" type="image/x-icon" />
<link rel="shortcut icon" href="images/home.png" type="image/x-icon" />
<style type="text/css">
div.img
{
  margin: 10px;
  border: 1px solid;
  height: 300px;
  width: 200px;
  float: left;
  text-align: center;
  background-image: url(images/bgBid.png);
	border-radius:3px 3px 3px 3px;
  border: 1px solid #C4C4C4;
}	
div.img img
{
  display: inline;
  width:  170px;
  height: 180px;
  border: 0px;
}
div.img a:hover img {border: 1px solid #C4C4C4;}
div.desc
{
  text-align: center;
  font-weight: normal;
  width: 200px;
  margin: 2px;
}
div.mgc{margin-left:120px;}
#time{
font-size:20px;
}
#prc{
font-size:17px;
}
.bidBtt{
	padding-top:1px;
	padding-bottom:1px;
	background-color:#DA4F49;
	border:0px;
	color:#FFFFFF;
	width:100px;
	height:25px;
	font-size:16px;
	border-radius:12px 12px 12px 12px;
	cursor:pointer;
}
#title{
font-size:17px;
}

</style>
<link rel="stylesheet" href="css/main.css" type="text/css">
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
	var currentImage;
    var currentIndex = -1;
    var interval;
    var index=0;
    function showImage(index){
    	$("#adlink").attr("href",$("#link"+index).val());
        if(index < $('#bigPic img').length){
        	var indexImage = $('#bigPic img')[index];
            if(currentImage){   
            	if(currentImage != indexImage ){
                    $(currentImage).css('z-index',2);
                    clearTimeout(myTimer);
                    $(currentImage).fadeOut(250, function() {
					    myTimer = setTimeout("showNext()", 3000);
					    $(this).css({'display':'none','z-index':1});
					});
                }
            }
            $(indexImage).css({'display':'block', 'opacity':1});
            currentImage = indexImage;
            currentIndex = index;
            $('#thumbs li').removeClass('active');
            $($('#thumbs li')[index]).addClass('active');
        }
    }
    
    function showNext(){
        var len = $('#bigPic img').length;
        var next = currentIndex < (len-1) ? currentIndex + 1 : 0;
        showImage(next);
    }
    
    var myTimer;
    
    $(document).ready(function() {
	    myTimer = setTimeout("showNext()", 3000);
		showNext(); //loads first image
        $('#thumbs li').bind('click',function(e){
        	var count = $(this).attr('rel');
        	showImage(parseInt(count)-1);
        });
        $("#field").keyup(function() {
    		$("#x").fadeIn();
    		if ($.trim($("#field").val()) == "") {
    			$("#x").fadeOut();
    		}
    	});
    	// on click of "X", delete input field value and hide "X"
    	$("#x").click(function() {
    		$("#field").val("");
    		$(this).hide();
    	});
    	$("#create").click(function(){
    		document.location.href='utp-bid-create.jsp';
    	});
    	
    	$("#bidBttConfirm").click(function(){
    		<%if(uif1==null){%>
    			$("#bidSMS").html("<center>Please login to do Bidding</center>");
    			return;
    		<%}%>
    		$("#bidConfirm").css("border","1px solid #3079ED");
    		$("#bidConfirm").removeAttr("disabled");
    		$("#bidBttConfirm").attr("disabled","disabled");
    		
    		var arrPrc=$("#md_Price").html().trim().split(" ");
    		var newPrc=parseFloat($("#svPrc").val().trim())+0.1;
			$("#svPrc").val(newPrc.toFixed(1));
			$("#md_Price").html(arrPrc[0]+" "+$("#svPrc").val());
    	});
    	
    	$("#bidConfirm").click(function(){
    		$.ajax({url:"ajax/update-bid.jsp?trnsNo="+$("#trnsNo").val(),success:function(){
    			$('.window .cancelBtt').click();
    			location.href="home.jsp";
    		  }});
    	});
    	
    	/*****************************************/
    	//select all the a tag with name equal to modal
    	
    	$('.bidBtt').click(function() {
    		var trnsNo=$(this).attr("id");
    		$.ajaxSetup({ cache: false });
    		$.getJSON("ajax/get-JSON-bid.jsp?trnsNo="+trnsNo, function(res){
    			$("#md_Name").html(res[0].name);
    			$("#md_Price").html(res[0].unit+" "+res[0].price);
    			$("#svPrc").val(res[0].price);
    			$("#trnsNo").val(trnsNo);
    			$("#md_Phone").html("<i>"+res[0].phone+"</i>");
    			$("#md_BidNum").html(res[0].bidNum);
    			$("#md_expr").html($("#cntdwn"+trnsNo).html());
    			var imgLoc="image/"+res[0].img+"?location="+"<%=DataLocation.bidDing%>"+"&trnsNo="+trnsNo;
    			$("#md_Img").attr("src", imgLoc);
    			
    			//Get the A tag
        		var id = "#dialog";
        	
        		//Get the screen height and width
        		var maskHeight = $(document).height();
        		var maskWidth = $(window).width();
        	
        		//Set heigth and width to mask to fill up the whole screen
        		$('#mask').css({'width':maskWidth,'height':maskHeight});
        		
        		//transition effect		
        		$('#mask').fadeIn(1000);	
        		$('#mask').fadeTo("slow",0.8);	
        	
        		//Get the window height and width
        		var winH = $(window).height();
        		var winW = $(window).width();
                      
        		//Set the popup window to center
        		$(id).css('width',  "645px");
        		$(id).css('height', "405px");
        		$(id).css('top',  winH/2-$(id).height()/2);
        		$(id).css('left', winW/2-$(id).width()/2);
        	
        		//transition effect
        		$(id).fadeIn(1000);

    		 }); 
    	});
    	
    	//if close button is clicked
    	$('.window .cancelBtt').click(function (e) {
    		//Cancel the link behavior
    		e.preventDefault();
    		
    		$('#mask').hide();
    		$('.window').hide();
    		$("#bidConfirm").attr("disabled","disabled");
    		$("#bidBttConfirm").removeAttr("disabled");
    	});		
    	
    	//if mask is clicked
    	$('#mask').click(function () {
    		$(this).hide();
    		$('.window').hide();
    		$("#bidConfirm").attr("disabled","disabled");
    		$("#bidBttConfirm").removeAttr("disabled");
    	});			

    	$(window).resize(function () {
    	 
     		var box = $('#boxes .window');
     
            //Get the screen height and width
            var maskHeight = $(document).height();
            var maskWidth = $(window).width();
          
            //Set height and width to mask to fill up the whole screen
            $('#mask').css({'width':maskWidth,'height':maskHeight});
                   
            //Get the window height and width
            var winH = $(window).height();
            var winW = $(window).width();

            //Set the popup window to center
            box.css('top',  winH/2 - box.height()/2);
            box.css('left', winW/2 - box.width()/2);
    	 
    	});
	});
	</script>
</head>
<body>
    <div class="mgc">
   <%@ include file="header.jsp" %>
<%
String [] fieldLst=null;
String field=request.getParameter("field");
if(field!=null){
	field=field.trim();
	if(field.length()>0)
		fieldLst=field.split(" ");
}
%>
      <div class="mgc-body">
        <div id='wrapper'>
		<div id='body' style="width:860px;">
			<div id="bigPic"><a id="adlink" href="">
				<img src="imgs/1.jpg" alt="" />
				<img src="imgs/2.jpg" alt="" />
				<img src="imgs/3.jpg" alt="" />
				<img src="imgs/4.jpg" alt="" />
				<img src="imgs/5.jpg" alt="" />
				<img src="imgs/6.jpg" alt="" /></a>
			</div>
			<ul id="thumbs">
				<li class='active' rel='1'>1</li>
				<li rel='2'>2</li>
				<li rel='3'>3</li>
				<li rel='4'>4</li>
				<li rel='5'>5</li>
				<li rel='6'>6</li>
			</ul>
			<input type="hidden" id="link0" value="http://deals.ebay.com/5000106358_Garmin_nuvi_2360LMT_4_3__GPS_w__Lifetime_Map"/>
			<input type="hidden" id="link1" value="http://www.ebay.com/"/>
			<input type="hidden" id="link2" value="http://www.amazon.fr/gp/product/B005890FUI/ref=amb_link_164260407_3"/>
			<input type="hidden" id="link3" value="http://www.avenue86.com/mall/"/>
			<input type="hidden" id="link4" value="http://www.ebay.com/itm/iPad-Wi-Fi-Cellular-16GB-Black-3rd-generation-UNLOCKED-Version-/251098973420"/>
			<input type="hidden" id="link5" value="http://www.uno.com.my/"/>
			<form method="post">
				<div style="float:left;width:30px;">&nbsp;</div>
				<input type="text" name="field" id="field" style="float:left;width:350px;"/><div id="delete"><span id="x">x</span></div>
				<input type="submit" maxlength="300" size="50" name="submit" id="submitSearch" value="Search" style="float:left;"/>
			<div style="float:left;width:20px;">&nbsp;</div>
			</form>
			<%if(uif!=null&&uif.isAdmin()){%>
				<input type="button" id="create" class="cancelBtt"
							style="width:90px;height:30px;background-color:#DA4F49;float:right;" value="Create+"/>
			<%} %>
		</div>
		<div class='clearfix'></div>
		<div id='push'></div>
	</div>
	<%
	String sql="SELECT bld_tran_no, bld_tran_nme, bld_tran_prc, bld_tran_xpr_dte, bld_tran_file_nme, bld_tran_usr_id, bld_tran_bid_num, bld_tran_unit "+
			"from bltran ";
	if(fieldLst!=null&&fieldLst.length>0){
	    sql+="where ";
		for(int i=0;i<fieldLst.length;i++){
			if(fieldLst[i].trim().length()>0){
				if(i>0) sql+="or ";
				sql+="bld_tran_nme like '%"+fieldLst[i].trim()+"%' ";
			}
		}
	}
	sql+="order by bld_tran_bid_num desc, bld_tran_xpr_dte";
	List<String[]> rs=SqlQuery.sqlSelect(sql);
	int size=8;
	int pageNo=rs.size()%size==0?rs.size()/size:rs.size()/size+1, start, end;
	String pageNum=request.getParameter("pageNo");
	if(pageNum==null){
		  start=0;
		  end=rs.size()<size?rs.size():size;
	}
	else{
		  start=Integer.parseInt(pageNum)*size;
		  end=rs.size()<start+size?rs.size():start+size;
	}
	String trsNo, name, price, exprDte, imgNme, imgLoc;
	String [] dte;
	for(int i=start;i<end;i++){
        	   trsNo=rs.get(i)[0];
        	   name=rs.get(i)[1];
        	   price=rs.get(i)[2];
        	   exprDte=rs.get(i)[3];
        	   dte=exprDte.split("-");
        	   exprDte=dte[1]+"/"+dte[2]+"/"+dte[0]+" 11:59 PM";
        	   imgNme=rs.get(i)[4];
        	   imgLoc="image/"+imgNme+"?location="+DataLocation.bidDing+"&trnsNo="+trsNo;
        	  
        	  String usrId=rs.get(i)[5];
        	  String bidNum=rs.get(i)[6];
        	  String unit=rs.get(i)[7];
        	  price=unit+" "+price;
        	  if(bidNum.equals("0")) usrId="&nbsp;";
          %>
        	  <div class="img"><center>
        	  <span id="title"><%=name%></span></center>
 			  <img src="<%=imgLoc%>" id="<%=trsNo %>" width="110" height="90" class="bidBtt" style="margin: 0;padding: 0;border-radius: 0;outline: 0;background: transparent;"/>
 			  <div class="desc"><br/>
 			  	<ul style="list-style-type: none;">
 			  		<li id="prc"><%=price%></li>
 			  		<li><%=usrId %></li>
 			  		<li><input type="button" id="<%=trsNo %>" class="bidBtt" value="Buy"/></li>
 			  	</ul>
 			  </div>
 			  </div>
          <%}
          %>
      <div id="showAdBid" style="clear:both;margin-top:20px">
	<table width="100%" style="background-color:#F3F7FB">
		<tr><td>&nbsp;</td></tr>
		<tr>
		<%
		   int nextPage=start/size+1>=pageNo?start/size:start/size+1;
		   int prevPage=start/size-1<0?start/size:start/size-1;
		%>
			<td valign="top" align="left" width="10%"><a href="?pageNo=0" style="color:blue">First Page</a></td>
			<td width="1%"><a href="?pageNo=<%=prevPage%>"><img src="data_images/prev.png"/></a></td>
			<%
				for(int i=0;i<pageNo;i++){
					String style="color:blue";
					if(i==start/size) style="color:red";
					out.print("<td width=\"1%\"><a  style=\""+style+"\" href=\"?pageNo="+i+"\">"+(i+1)+"</a></td>");
				}
			%>
			<td width="2%"><a href="?pageNo=<%=nextPage%>"><img src="data_images/next.png"/></a></td>
			<td valign="top" align="right" width="10%"><a href="?pageNo=<%=pageNo-1 %>" style="color:blue;float:right">Last Page</a></td>
		</tr>
	</table>
</div>
      </div>
	  <div style="height:700px;">&nbsp;</div>
      <div class="g-section footer">
        <p class="fineprint">
          ©2012 Copy Right By SHBSC
        </p>
      </div>
    </div>
    <!---------------------------------------->
    <div id="boxes">
		<div id="dialog" class="window">
			<div id="product_image_frame">
				<img id="md_Img" src=""/>
			</div>
			<div id="middleblock">
				<div class="auction-detail-bid-frame">
				<table style="width:295px;font-size:1.1em">
				<tr><th colspan="2" class="auction-name-header" id="md_Name">&nbsp;</th></tr>
				<tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                  <td style="font-weight:bold">Price</td>
                  <td>
                    <div style="font-size:1.5em;font-weight:bold;font-family:Verdana;color:#66BC29" id="md_Price">&nbsp;</div>
                  </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                  <td style="font-weight:bold">Contact</td>
                  <td>
                    <div style="font-size:1.2em" id="md_Phone">&nbsp;</div>
                  </td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                  <td colspan="2" align="center">
                    <div style="font-size:1.5em;font-weight:bold;font-family:Verdana;color:red" id="md_expr">&nbsp;
                    </div>
                  </td>
                </tr>
                <tr><td colspan="2" id="bidSMS" style="color:red">&nbsp;</td></tr>
              </table>
				</div>
				<div class="discount_frame_block"><center>
					<table style="width:295px;font-size:1.1em">
					<tr><td>&nbsp;</td></tr>
						<tr><td align="center"><input type="button" id="bidConfirm" disabled
							style="width:90px;height:30px;background-color:blue;color:white" value="Order"/>&nbsp;
								<input type="button" class="cancelBtt"
							style="width:90px;height:30px;background-color:#DA4F49;" value="Cancel"/>
							</td>
							</tr>
					</table></center>
				</div>
			</div>
		</div>
			<!-- Mask to cover the whole screen -->
		 <div id="mask"></div>
	</div>
 </body>
</html>