$(document).ready(function(){
	$("#usrPst").focus(function(){
		if($("#usrPst").height()!="60"&&$("#usrPst").val().length==0){
			$("#showAd").height("60px");
			$("#usrPst").height($("#showAd").height());
		}
	});
	
	$("#usrPst").blur(function(){
		if($("#usrPst").val().length==0){
			$("#showAd").height("28px");
			$("#usrPst").height($("#showAd").height());
		}
	});
	
	$("#usrPst").keyup(function(){
		if($("#usrPst").height()<60)
		{
			$("#showAd").height("60px");
			$("#usrPst").height($("#showAd").height());
		}
	});
	
	$(window).load(function(){
		if($("#usrPst").val().length>0&&$("#usrPst").height()<60){
			$("#showAd").height("60px");
			$("#usrPst").height($("#showAd").height());
		}
	});
});
