package SHBSC.form;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import SHBSC.com.DataLocation;

public class ImageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final int DEFAULT_BUFFER_SIZE = 1024; // 10KB.

    
    public void init() throws ServletException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {	
        String location=request.getParameter("location");
        String usrNo=request.getParameter("usrNo");
        File image=null;
        

    	String requestedImage = request.getPathInfo().replace("/", "");
        if(location.equals(DataLocation.rentCar))
        	image = new File(DataLocation.USER_DATA_INFO+usrNo+DataLocation.CAR_UPLOAD_IMG+requestedImage);
        else if(location.equals(DataLocation.sellGood))
        	image = new File(DataLocation.USER_DATA_INFO+usrNo+DataLocation.SELL_UPLOAD_IMG+requestedImage);
        else if(location.equals(DataLocation.bidDing)){
        	String trnsNo=request.getParameter("trnsNo");
        	image = new File(DataLocation.BID_DATA+requestedImage);
        	
        }
        	
        
        response.reset();
        response.setBufferSize(DEFAULT_BUFFER_SIZE);
        response.setHeader("Content-Length", String.valueOf(image.length()));
        response.setHeader("Content-Disposition", "inline; filename=\"" + image.getName() + "\"");
        response.setHeader("Cache-Control","no-cache"); 
		response.setHeader("Pragma","no-cache");
		response.setDateHeader ("Expires", 0);

        BufferedInputStream input = null;
        BufferedOutputStream output = null;

        try {
            input = new BufferedInputStream(new FileInputStream(image), DEFAULT_BUFFER_SIZE);
            output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);

            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
        } finally {
            close(output);
            close(input);
        }
    }

    private static void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}