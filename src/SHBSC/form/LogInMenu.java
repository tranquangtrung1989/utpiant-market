package SHBSC.form;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import SHBSC.com.UserInfo;

public class LogInMenu extends HttpServlet{
	private static final long serialVersionUID = 1L;
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("yesssss");
		response.setHeader("Cache-Control","no-cache"); 
		response.setHeader("Pragma","no-cache");
		response.setDateHeader ("Expires", 0);
 
			try {
				String userID=request.getParameter("login_user");
				String password=request.getParameter("password");
				
				String sql="select syu_usr_no, syu_usr_id, syu_usr_email_id, syu_usr_typ from sysusr "+
				           "where syu_usr_email_id='"+userID+"' and syu_usr_pwd='"+password+"' ";
				List<String[]> rs=conn.SqlQuery.sqlSelect(sql);
				if(rs.size()>0){
					String userNo=rs.get(0)[0];
					String dispName=rs.get(0)[1];
					String email=rs.get(0)[2];
					String usrTyp=rs.get(0)[3];
					UserInfo uif=new UserInfo();
					uif.setEmail(email);
					uif.setUserID(dispName);
					uif.setUserNo(userNo);
					uif.setUserTyp(usrTyp);
					HttpSession ses = request.getSession();
					ses.setAttribute("UserInfo", uif);
				}
				String nextJSP = request.getParameter("currentLoc").replace("/UTP", "");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
				dispatcher.forward(request,response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	public void destroy() {
		 
	}
}
