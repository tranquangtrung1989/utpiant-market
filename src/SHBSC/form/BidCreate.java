package SHBSC.form;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import SHBSC.com.DataLocation;
import SHBSC.com.UserInfo;
import SHBSC.img.ImageResizer;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;

public class BidCreate extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	private String getMonth(String s){
		if(s.equals("JAN")) return "1";
		if(s.equals("FEB")) return "2";
		if(s.equals("MAR")) return "3";
		if(s.equals("APR")) return "4";
		if(s.equals("MAY")) return "5";
		if(s.equals("JUN")) return "6";
		if(s.equals("JUL")) return "7";
		if(s.equals("AUG")) return "8";
		if(s.equals("SEP")) return "9";
		if(s.equals("OCT")) return "10";
		if(s.equals("NOV")) return "11";
		return "12";
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Cache-Control","no-cache"); 
		response.setHeader("Pragma","no-cache");
		response.setDateHeader ("Expires", 0);
 
		HttpSession session = request.getSession();
		final String contextPath = request.getContextPath();
		UserInfo uif = (UserInfo) session.getAttribute("UserInfo");
		if (uif == null) {
			response.sendRedirect(contextPath);
			return;
		}
		try {
			MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
			String title=mrequest.getParameter("title");
			String price=mrequest.getParameter("price");
			String unit=mrequest.getParameter("unit");
			String phone=mrequest.getParameter("phone");
			String exprDte=mrequest.getParameter("exprDte");
			String []dte=exprDte.split("-");
			exprDte=dte[2]+"-"+getMonth(dte[1])+"-"+dte[0];
			String sql="select ifnull(max(bld_tran_no),0) from bltran";
			Integer  maxSeqNo=0;
			maxSeqNo=Integer.parseInt(conn.SqlQuery.sqlSelect(sql).get(0)[0])+1;
			
			File fileUpload = new File(DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.UPLOAD_IMG_TEMP);
			if(!fileUpload.exists()){
				fileUpload.mkdirs();
			}
			fileUpload = new File(DataLocation.BID_DATA+maxSeqNo+"/");
			if(!fileUpload.exists()){
				fileUpload.mkdirs();
			}
			
			UploadBean upBean=new UploadBean();
	        upBean.setFolderstore(DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.UPLOAD_IMG_TEMP);
            Hashtable<?, ?> files = mrequest.getFiles();
            if ( (files != null) && (!files.isEmpty()) )
            {
                UploadFile file = (UploadFile) files.get("img");
                if(file!=null&&file.getFileSize()>0)
                {
                    upBean.store(mrequest, "img");
                    ImageResizer imrs=new ImageResizer();
                    imrs.resizeImage(DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.UPLOAD_IMG_TEMP+file.getFileName(),
                    		DataLocation.BID_DATA+maxSeqNo+"/"+file.getFileName(),340,400);
                    upBean.store(mrequest, "img");
                    imrs.resizeImage(DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.UPLOAD_IMG_TEMP+file.getFileName(),
                    		DataLocation.BID_DATA+maxSeqNo+"/"+"small"+file.getFileName(),110,90);
                }
                sql="insert into bltran (bld_tran_no, bld_tran_nme, bld_tran_prc, bld_tran_unit, bld_tran_phone, bld_tran_xpr_dte, bld_tran_file_nme, bld_tran_bid_num) "+
 			           "values ('"+maxSeqNo+"','"+title+"','"+price+"','"+unit+"', '"+phone+"','"+exprDte+"','"+file.getFileName()+"','0')";
                conn.SqlQuery.sqlExe(sql);
            }
			
            String nextJSP = DataLocation.HOME_PAGE_LOC;
            response.sendRedirect(nextJSP);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void destroy() {
		 
	}
}
