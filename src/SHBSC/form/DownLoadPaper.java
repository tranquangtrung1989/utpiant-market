package SHBSC.form;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import SHBSC.com.DataLocation;
import SHBSC.com.UserInfo;

public class DownLoadPaper extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Cache-Control","no-cache"); 
		response.setHeader("Pragma","no-cache");
		response.setDateHeader ("Expires", 0);
 
		HttpSession session = request.getSession();
		UserInfo uif = (UserInfo) session.getAttribute("UserInfo");
		if (uif == null) {
			response.sendRedirect(DataLocation.UPLOAD_DOWNLOAD_RESOURCE);
			return;
		}
		
			String filename=request.getParameter("fileName");
			String filepath=DataLocation.PAPER_EXAMP+request.getParameter("dwnCourse")+"/";

			BufferedInputStream buf=null;
			     ServletOutputStream myOut=null;
			try{
			     myOut = response.getOutputStream( );
			     File myfile = new File(filepath+filename);
			     response.setContentType("text/plain");
			     response.addHeader("Content-Disposition","attachment; filename="+filename );
			     response.setContentLength( (int) myfile.length( ) );
			     FileInputStream input = new FileInputStream(myfile);
			     buf = new BufferedInputStream(input);
			     int readBytes = 0;
			     while((readBytes = buf.read( )) != -1)
			         myOut.write(readBytes);
			     String sql="update crsehf set crs_utp_dwnload=crs_utp_dwnload+1 where crs_utp_cde='"+request.getParameter("dwnCourse")+"' ";
			     conn.SqlQuery.sqlExe(sql);

			} catch (Exception ioe){
			        throw new ServletException(ioe.getMessage( ));
			} finally {
			         if (myOut != null)
			             myOut.close( );
			         if (buf != null)
			             buf.close( );
			     }
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	public void destroy() {
		 
	}
}
