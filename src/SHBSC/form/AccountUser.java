package SHBSC.form;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import SHBSC.com.DataLocation;
import SHBSC.com.UserInfo;
import SHBSC.img.ImageResizer;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;

public class AccountUser extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Cache-Control","no-cache"); 
		response.setHeader("Pragma","no-cache");
		response.setDateHeader ("Expires", 0);
 
		String processClock=new String();
		synchronized(processClock){
			try {
				MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
				String email=mrequest.getParameter("email");
				String dispName=mrequest.getParameter("dispName");
				String day=mrequest.getParameter("day");
				String month=mrequest.getParameter("month");
				String year=mrequest.getParameter("year");
				String gender=mrequest.getParameter("gender");
				String birthDate=year+'-'+month+'-'+day;
				String usrTyp="U";
				String sql="";
				
				HttpSession session = request.getSession();
				UserInfo uif = (UserInfo) session.getAttribute("UserInfo");
				
				if (uif == null) {
					sql="select 1 from sysusr where syu_usr_email_id='"+email+"' ";
					if(conn.SqlQuery.sqlSelect(sql).size()>0){
						response.sendRedirect(DataLocation.REGISTER_PAGE_LOC+"?warnSms=email");
						return;
					}
					sql="select ifnull(max(syu_usr_no),0) from sysusr";
					Integer  maxSeqNo=Integer.parseInt(conn.SqlQuery.sqlSelect(sql).get(0)[0])+1;
					
					sql="insert into sysusr (syu_usr_no, syu_usr_email_id, syu_usr_id, syu_usr_pwd, syu_usr_gender, "+
					    " syu_usr_birth_dte, syu_lst_upd_dte, syu_usr_typ) "+
					    "values ('"+maxSeqNo+"', '"+email+"','"+dispName+"','"+mrequest.getParameter("password")+"', "+
					    "'"+gender+"','"+birthDate+"',now(),'"+usrTyp+"') ";
					conn.SqlQuery.sqlExe(sql);
					
					sql="select syu_usr_no, syu_usr_id from sysusr where syu_usr_no='"+maxSeqNo+"' ";
					String[] rs=conn.SqlQuery.sqlSelect(sql).get(0);
					String userNo=rs[0];
					dispName=rs[1];
					uif=new UserInfo();
					uif.setUserNo(userNo);
					HttpSession ses = request.getSession();
					ses.setAttribute("UserInfo", uif);
					File file = new File(DataLocation.USER_DATA_INFO+userNo);
				    file.mkdirs();
				    file = new File(DataLocation.USER_DATA_INFO+userNo+DataLocation.CAR_UPLOAD_IMG);
				    file.mkdirs();
				    file = new File(DataLocation.USER_DATA_INFO+userNo+DataLocation.SELL_UPLOAD_IMG);
				    file.mkdirs();
				    file = new File(DataLocation.USER_DATA_INFO+userNo+DataLocation.UPLOAD_IMG_TEMP);
				    file.mkdirs();
				    file = new File(DataLocation.USER_DATA_INFO+userNo+DataLocation.PROFILE_PICTURE);
				    file.mkdirs();
				}else{
					sql="select 1 from sysusr where syu_usr_no='"+uif.getUserNo()+"' and syu_usr_pwd='"+mrequest.getParameter("currentPassword")+"' ";
					if(conn.SqlQuery.sqlSelect(sql).size()==0){
						response.sendRedirect(DataLocation.REGISTER_PAGE_LOC+"?warnSms=password");
						return;
					}
					sql="update sysusr set syu_usr_email_id='"+email+"', syu_usr_id='"+dispName+"', "+
					    "syu_usr_pwd='"+mrequest.getParameter("newPassword")+"', "+
					    "syu_usr_gender='"+gender+"', syu_usr_birth_dte='"+birthDate+"', syu_lst_upd_dte=now() "+
					    "where syu_usr_no='"+uif.getUserNo()+"' ";
					conn.SqlQuery.sqlExe(sql);
				}
				
				uif.setEmail(email);
				uif.setUserID(dispName);
				uif.setUserTyp(usrTyp);
				String saveImageName=uif.getUserNo()+".png";
				UploadBean upBean=new UploadBean();
		        upBean.setFolderstore(DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.UPLOAD_IMG_TEMP);
	            Hashtable<?, ?> files = mrequest.getFiles();
	            if ( (files != null) && (!files.isEmpty()) )
	            {
	                UploadFile uploadFile = (UploadFile) files.get("img");
	                if(uploadFile!=null&&uploadFile.getFileSize()>0)
	                {
	                	File currentImg=new File(DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.PROFILE_PICTURE+saveImageName);
	                	currentImg.delete();
	                    upBean.store(mrequest, "img");
	                    ImageResizer imrs=new ImageResizer();
	                    imrs.resizeImage(DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.UPLOAD_IMG_TEMP+uploadFile.getFileName(),
	                    		DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.PROFILE_PICTURE+saveImageName,133,100);
	                }
	            }
	            String nextJSP = DataLocation.HOME_PAGE_LOC;
	            response.sendRedirect(nextJSP);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void destroy() {
		 
	}
}
