package SHBSC.form;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import SHBSC.com.DataLocation;
import SHBSC.com.UserInfo;
import SHBSC.img.ImageResizer;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;

public class CarProcess extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Cache-Control","no-cache"); 
		response.setHeader("Pragma","no-cache");
		response.setDateHeader ("Expires", 0);
 
		HttpSession session = request.getSession();
		final String contextPath = request.getContextPath();
		UserInfo uif = (UserInfo) session.getAttribute("UserInfo");
		if (uif == null) {
			response.sendRedirect(contextPath);
			return;
		}
		try {
			MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
			String submitTyp=mrequest.getParameter("submitTyp");
			String title=mrequest.getParameter("title");
			String price=mrequest.getParameter("price");
			String unit=mrequest.getParameter("unit");
			if(unit.length()>0)price+=unit;
			String phone=mrequest.getParameter("phone");
			String mail=mrequest.getParameter("mail");
			String usrNo=uif.getUserNo();
			String saveImageName="";
			String sql="select ifnull(max(crt_tran_no),0) from crtran";
			Integer  maxSeqNo=0;
			if(submitTyp.equals("edit")){
				maxSeqNo=Integer.parseInt(mrequest.getParameter("trnsNo"));
				saveImageName=mrequest.getParameter("orgImg");
				usrNo=mrequest.getParameter("usrNo");
			}
			else maxSeqNo=Integer.parseInt(conn.SqlQuery.sqlSelect(sql).get(0)[0])+1;
			
			File fileUpload = new File(DataLocation.USER_DATA_INFO+usrNo+DataLocation.UPLOAD_IMG_TEMP);
			if(!fileUpload.exists()){
				fileUpload.mkdirs();
			}
			fileUpload = new File(DataLocation.USER_DATA_INFO+usrNo+DataLocation.CAR_UPLOAD_IMG);
			if(!fileUpload.exists()){
				fileUpload.mkdirs();
			}
			
			UploadBean upBean=new UploadBean();
	        upBean.setFolderstore(DataLocation.USER_DATA_INFO+usrNo+DataLocation.UPLOAD_IMG_TEMP);
            Hashtable<?, ?> files = mrequest.getFiles();
            if ( (files != null) && (!files.isEmpty()) )
            {
                UploadFile file = (UploadFile) files.get("img");
                if(file!=null&&file.getFileSize()>0)
                {
                	if(submitTyp.equals("edit")){
                		fileUpload = new File(DataLocation.USER_DATA_INFO+usrNo+DataLocation.CAR_UPLOAD_IMG+saveImageName);
                		if(saveImageName.endsWith(".png")&&fileUpload.exists())fileUpload.delete();
                		else saveImageName=usrNo+"_"+maxSeqNo+".png";
                	}
                	else saveImageName=usrNo+"_"+maxSeqNo+".png";
                	
                    upBean.store(mrequest, "img");
                    ImageResizer imrs=new ImageResizer();
                    imrs.resizeImage(DataLocation.USER_DATA_INFO+usrNo+DataLocation.UPLOAD_IMG_TEMP+file.getFileName(),
                    		DataLocation.USER_DATA_INFO+usrNo+DataLocation.CAR_UPLOAD_IMG+saveImageName,133,100);
                }
            }
            if(submitTyp.equals("edit"))
            	sql="update crtran set crt_tran_nme='"+title+"' , crt_tran_prc='"+price+"', crt_tran_phone='"+phone+"', crt_tran_email='"+mail+"', "+
            		"crt_tran_usr_no='"+usrNo+"',crt_tran_file_nme='"+saveImageName+"', crt_lst_upd_dte=now() where crt_tran_no='"+maxSeqNo+"'";
            else
			sql="insert into crtran (crt_tran_no, crt_tran_nme, crt_tran_prc, crt_tran_phone, crt_tran_email, crt_tran_usr_no, crt_tran_file_nme, crt_lst_upd_dte) "+
			           "values ('"+maxSeqNo+"','"+title+"','"+price+"','"+phone+"','"+mail+"','"+usrNo+"','"+saveImageName+"',now())";
			conn.SqlQuery.sqlExe(sql);
			
            String nextJSP = DataLocation.RENT_CAR_DISP;
            response.sendRedirect(nextJSP);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void destroy() {
		 
	}
}
