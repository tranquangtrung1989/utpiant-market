package SHBSC.form;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import SHBSC.com.DataLocation;
import SHBSC.com.UserInfo;


public class DeleteSellTrans extends HttpServlet{
	private static final long serialVersionUID = 1L;
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Cache-Control","no-cache"); 
		response.setHeader("Pragma","no-cache");
		response.setDateHeader ("Expires", 0);
 
		HttpSession session = request.getSession();
		final String contextPath = request.getContextPath();
		UserInfo uif = (UserInfo) session.getAttribute("UserInfo");
		if (uif == null) {
			response.sendRedirect(contextPath);
			return;
		}
			try {
				String trnsNo=request.getParameter("trnsNo");
				String imgNme=request.getParameter("imgNme");
				String usrNo=request.getParameter("usrNo");
				String sql="delete from sltran where sld_tran_no='"+trnsNo+"' ";
				if(imgNme.trim().length()>0){
					File file=new File(DataLocation.USER_DATA_INFO+usrNo+DataLocation.SELL_UPLOAD_IMG+imgNme);
					file.delete();
				}
				conn.SqlQuery.sqlExe(sql);
	            String nextJSP=DataLocation.SELL_GOODS_DISP;
	            response.sendRedirect(nextJSP);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	public void destroy() {
		 
	}
}
