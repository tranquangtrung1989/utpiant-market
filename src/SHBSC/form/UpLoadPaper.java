package SHBSC.form;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import SHBSC.com.DataLocation;
import SHBSC.com.UserInfo;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;

public class UpLoadPaper extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Cache-Control","no-cache"); 
		response.setHeader("Pragma","no-cache");
		response.setDateHeader ("Expires", 0);
		
		HttpSession session = request.getSession();
		UserInfo uif = (UserInfo) session.getAttribute("UserInfo");
		
		String usrNo=DataLocation.adminNo;
		if(uif!=null) usrNo=uif.getUserNo();
		
		try {
			MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
			String upCourse=mrequest.getParameter("upCourse");
			String semester=mrequest.getParameter("semester");
			String sql="";
			File fileUpload = new File(DataLocation.PAPER_EXAMP+upCourse+"/");
			if(!fileUpload.exists()){
				fileUpload.mkdirs();
				sql="update crsehf set crs_utp_sts='Y' where crs_utp_cde='"+upCourse+"' ";
				conn.SqlQuery.sqlExe(sql);
			}
			
			UploadBean upBean=new UploadBean();
			upBean.setFolderstore(DataLocation.PAPER_EXAMP+upCourse+"/");
            Hashtable<?, ?> files = mrequest.getFiles();
            if ( (files != null) && (!files.isEmpty()) )
            {
                UploadFile file = (UploadFile) files.get("upFile");
                if(file!=null&&file.getFileSize()>0)
                {
                	String fileName=semester+"_"+file.getFileName();
                	String [] names=fileName.split("[.]");
                	int count=1;
                	while(new File(DataLocation.PAPER_EXAMP+upCourse+"/"+fileName).exists()){
                		fileName=names[0]+(count++)+"."+names[1];
                	}
                	file.setFileName(fileName);
                    upBean.store(mrequest, "upFile");
                    sql="update crsehf set crs_utp_upload=crs_utp_upload+1 where crs_utp_cde='"+upCourse+"' ";
                    conn.SqlQuery.sqlExe(sql);
                    sql="insert into crsedf (crd_utp_cde, crd_utp_nme, crd_utp_sem, crd_utp_usr_cde, crd_lst_upd_dte) "
                       +"values('"+upCourse+"','"+fileName+"','"+semester+"','"+usrNo+"',now()) ";
                    conn.SqlQuery.sqlExe(sql);
         
                }
            }
			
            String nextJSP = DataLocation.UPLOAD_DOWNLOAD_RESOURCE;
            response.sendRedirect(nextJSP+"?dwnCourse="+upCourse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void destroy() {
		 
	}
}
