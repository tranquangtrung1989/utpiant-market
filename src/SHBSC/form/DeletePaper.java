package SHBSC.form;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import SHBSC.com.DataLocation;
import SHBSC.com.UserInfo;

public class DeletePaper extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Cache-Control","no-cache"); 
		response.setHeader("Pragma","no-cache");
		response.setDateHeader ("Expires", 0);
 
		HttpSession session = request.getSession();
		UserInfo uif = (UserInfo) session.getAttribute("UserInfo");
		if (uif == null||(!uif.isAdmin()&&!uif.getUserNo().equals(request.getParameter("usrNo")))) {
			response.sendRedirect(DataLocation.UPLOAD_DOWNLOAD_RESOURCE);
			return;
		}
		
		try {
			String filename=request.getParameter("fileName");
			String crsCde=request.getParameter("dwnCourse");
			String filepath=DataLocation.PAPER_EXAMP+crsCde+"/"+filename;
			
			File file=new File(filepath);
			if(file.exists()&&filename.trim().length()>0) file.delete();
			
			String sql="delete from  crsedf where crd_utp_cde='"+crsCde+"' and crd_utp_nme='"+filename+"'";	
			conn.SqlQuery.sqlExe(sql);
			
			sql="update crsehf set crs_utp_upload=crs_utp_upload-1 where crs_utp_cde='"+crsCde+"'";
			conn.SqlQuery.sqlExe(sql);
			
			response.sendRedirect(DataLocation.UPLOAD_DOWNLOAD_RESOURCE+"?dwnCourse="+crsCde);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	public void destroy() {
		 
	}
}
