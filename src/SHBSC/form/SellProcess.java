package SHBSC.form;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import SHBSC.com.DataLocation;
import SHBSC.com.UserInfo;
import SHBSC.img.ImageResizer;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;

public class SellProcess extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Cache-Control","no-cache"); 
		response.setHeader("Pragma","no-cache");
		response.setDateHeader ("Expires", 0);
 
		HttpSession session = request.getSession();
		final String contextPath = request.getContextPath();
		UserInfo uif = (UserInfo) session.getAttribute("UserInfo");
		if (uif == null) {
			response.sendRedirect(contextPath);
			return;
		}
		try {
			MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
			String submitTyp=mrequest.getParameter("submitTyp");
			String title=mrequest.getParameter("title");
			String price=mrequest.getParameter("price");
			String category=mrequest.getParameter("category");
			String phone=mrequest.getParameter("phone");
			String mail=mrequest.getParameter("mail");
			String desc=mrequest.getParameter("desc");
			String usrNo=uif.getUserNo();
			String saveImageName="";
			Integer  maxSeqNo=0;
			String sql="select ifnull(max(sld_tran_no),0) from sltran";
			if(submitTyp.equals("edit")){
				maxSeqNo=Integer.parseInt(mrequest.getParameter("trnsNo"));
				saveImageName=mrequest.getParameter("orgImg");
				usrNo=mrequest.getParameter("usrNo");
			}
			else maxSeqNo=Integer.parseInt(conn.SqlQuery.sqlSelect(sql).get(0)[0])+1;
			
			File fileUpload = new File(DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.UPLOAD_IMG_TEMP);
			if(!fileUpload.exists()){
				fileUpload.mkdirs();
			}
			fileUpload = new File(DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.SELL_UPLOAD_IMG);
			if(!fileUpload.exists()){
				fileUpload.mkdirs();
			}

			UploadBean upBean=new UploadBean();
	        upBean.setFolderstore(DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.UPLOAD_IMG_TEMP);
            Hashtable<?, ?> files = mrequest.getFiles();
            if ( (files != null) && (!files.isEmpty()) )
            {
                UploadFile file = (UploadFile) files.get("img");
                if(file!=null&&file.getFileSize()>0)
                {
                	if(submitTyp.equals("edit")){
                		fileUpload = new File(DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.SELL_UPLOAD_IMG+saveImageName);
                		if(saveImageName.endsWith(".png")&&fileUpload.exists())fileUpload.delete();
                		else saveImageName=uif.getUserNo()+"_"+maxSeqNo+".png";

                	}
                	else saveImageName=uif.getUserNo()+"_"+maxSeqNo+".png";
                    upBean.store(mrequest, "img");
                    ImageResizer imrs=new ImageResizer();
                    imrs.resizeImage(DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.UPLOAD_IMG_TEMP+file.getFileName(),
                    		DataLocation.USER_DATA_INFO+uif.getUserNo()+DataLocation.SELL_UPLOAD_IMG+saveImageName,133,100);
                }
            }

            if(submitTyp.equals("edit"))
            	sql="update sltran set sld_tran_nme='"+title+"' , sld_tran_prc='"+price+"', sld_tran_typ='"+category+"', sld_tran_phone='"+phone+"', sld_tran_email='"+mail+"', "+
            		"sld_tran_desc='"+desc+"', sld_tran_usr_no='"+usrNo+"',sld_tran_file_nme='"+saveImageName+"', sld_lst_upd_dte=now() where sld_tran_no='"+maxSeqNo+"'";
            else sql="insert into sltran (sld_tran_no, sld_tran_nme, sld_tran_prc, sld_tran_typ, sld_tran_phone, sld_tran_email, sld_tran_desc, sld_tran_usr_no, sld_tran_file_nme, sld_lst_upd_dte) "+
			           "values ('"+maxSeqNo+"','"+title+"','"+price+"','"+category+"','"+phone+"','"+mail+"','"+desc+"','"+usrNo+"','"+saveImageName+"',now())";
			conn.SqlQuery.sqlExe(sql);
			
            String nextJSP = DataLocation.SELL_GOODS_DISP;
            response.sendRedirect(nextJSP);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void destroy() {
		 
	}
}
