package SHBSC.com;

public class UserInfo {
	private String email;
	private String userID;
	private String userNo;
	private String userTyp;
	
	public void setEmail(String email){
		this.email=email;
	}
	public void setUserID(String userID){
		this.userID=userID;
	}
	
	public void setUserNo(String userNo){
		this.userNo=userNo;
	}
	public void setUserTyp(String userTyp){
		this.userTyp=userTyp;
	}
	
	public String getUserNo(){
		return this.userNo;
	}
	public String getEmail(){
		return this.email;
	}
	public String getUserID(){
		return this.userID;
	}
	
	public boolean isAdmin(){
		if(this.userTyp.equals("A")) return true;
		return false;
	}
}
